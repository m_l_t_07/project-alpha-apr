from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm
from tasks.models import Task


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            new_task = form.save(commit=False)
            new_task.save()
            return redirect("project_list")
    else:
        form = TaskForm()

    context = {"form": form}
    return render(request, "tasks/create_task.html", context)


@login_required
def show_my_tasks(request):
    user_tasks = Task.objects.filter(assignee=request.user)
    context = {"tasks": user_tasks}
    return render(request, "tasks/show_my_tasks.html", context)


# Create your views here.
