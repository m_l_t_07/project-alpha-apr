from django.urls import path
from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from accounts.forms import LoginForm, SignupForm
from django.contrib.auth import authenticate, login, logout


def signup_view(request):
    if request.method == "POST":
        form = SignupForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            if password == password_confirmation:
                user = UserCreationForm(
                    data={
                        "username": username,
                        "password1": password,
                        "password2": password_confirmation,
                    }
                )
                if user.is_valid():
                    user.save()
                    user = authenticate(
                        request, username=username, password=password
                    )
                    if user is not None:
                        login(request, user)
                        return redirect("list_projects")
            else:
                form.add_error(
                    "password_confirmation", "The passwords do not match."
                )
    else:
        form = SignupForm()

    return render(request, "accounts/signup.html", {"form": form})


def login_view(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("list_projects")
    else:
        form = LoginForm()

    return render(request, "accounts/login.html", {"form": form})


def logout_view(request):
    logout(request)
    return redirect("login")


urlpatterns = [
    path("login/", login_view, name="login"),
    path("logout/", logout_view, name="logout"),
    path("signup/", signup_view, name="signup"),
]
# Create your views here.
